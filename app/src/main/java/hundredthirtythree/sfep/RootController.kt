package hundredthirtythree.sfep

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bluelinelabs.conductor.Controller

/**
 * Created by chingizh on 3/2/18.
 */

class RootController : Controller() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        return inflater.inflate(R.layout.controller_root, container, false)
    }

}