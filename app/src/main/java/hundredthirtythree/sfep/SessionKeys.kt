package hundredthirtythree.sfep

enum class SessionKeys private constructor(val key: String) {

    PREFS_NAME("sfep"),
    IP("ip"),
    PORT("port")
}