package hundredthirtythree.sfep

import android.app.Application

import hundredthirtythree.sessionmanager.SessionManager

class SFEP : Application() {
    override fun onCreate() {
        super.onCreate()
        SessionManager.Builder()
                .setContext(applicationContext)
                .setPrefsName(SessionKeys.PREFS_NAME.key)
                .build()
    }
}
