package hundredthirtythree.sfep

import android.content.ContentValues.TAG
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.bluelinelabs.conductor.Controller
import com.google.zxing.Result
import hundredthirtythree.sessionmanager.SessionManager
import me.dm7.barcodescanner.zxing.ZXingScannerView
import okhttp3.*
import org.json.JSONObject
import java.io.IOException

/**
 * Created by chingizh on 3/2/18.
 */

class ScanController : Controller(), ZXingScannerView.ResultHandler {

    private var mScannerView: ZXingScannerView? = null
    private var green: RelativeLayout? = null
    private var red: RelativeLayout? = null
    private var handler: Handler? = null
    private val client = OkHttpClient()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val v = inflater.inflate(R.layout.controller_passed, container, false)
        mScannerView = v.findViewById(R.id.scannerView)
        green = v.findViewById(R.id.green)
        red = v.findViewById(R.id.red)
        handler = android.os.Handler(Looper.getMainLooper())
        return v
    }

    override fun handleResult(rawResult: Result?) {
        // Do something with the result here
        Log.v(TAG, rawResult?.getText()) // Prints scan results
        Log.v(TAG, rawResult?.getBarcodeFormat().toString()) // Prints the scan format (qrcode, pdf417 etc.)
        if (rawResult != null)
            run(rawResult.text)

        // If you would like to resume scanning, call this method below:
        // mScannerView?.resumeCameraPreview(this)
    }

    private fun run(pid: String) {
        val ip = SessionManager.getString(SessionKeys.IP.key, "")
        val port = SessionManager.getString(SessionKeys.PORT.key, "")
        val request = Request.Builder()
                .url("http://$ip:$port/$pid")
                .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {}
            override fun onResponse(call: Call, response: Response) {
                var json = JSONObject(response.body()?.string())
                val result = json.getBoolean("result")
                handler?.post({ doThings(result) })
//                doThings(result)
//                println(response.body()?.string())
            }
        })
    }

    fun doThings(result: Boolean) {
        mScannerView?.visibility = View.GONE
        if (result) {
            green?.visibility = View.VISIBLE
            red?.visibility = View.GONE
        } else {
            green?.visibility = View.GONE
            red?.visibility = View.VISIBLE
        }
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        mScannerView?.setResultHandler(this)
        mScannerView?.startCamera()
    }

    override fun onDetach(view: View) {
        super.onDetach(view)
        mScannerView?.stopCamera()
    }
}