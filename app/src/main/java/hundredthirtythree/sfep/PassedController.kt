package hundredthirtythree.sfep

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bluelinelabs.conductor.Controller

/**
 * Created by chingizh on 3/30/18.
 */

class PassedController: Controller() {

    companion object {
        fun newInstance(result: Boolean) : PassedController {
            val b = Bundle()
            b.putBoolean("result", result)
            return PassedController()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val v = inflater.inflate(R.layout.controller_passed, container, false)
        println("elde olunan arg"+args.getBoolean("result"))
        return v
    }

}