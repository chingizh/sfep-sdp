package hundredthirtythree.sfep

import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import hundredthirtythree.sessionmanager.SessionManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    lateinit var router: Router

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        router = Conductor.attachRouter(this, controller_container, savedInstanceState)
        if (!router.hasRootController()) {
            router.setRoot(RouterTransaction.with(RootController()))
        }

        fab.setOnClickListener { view ->
            router.pushController(RouterTransaction.with(ScanController())
                    .pushChangeHandler(FadeChangeHandler()))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return if (item.itemId == R.id.action_change_link) {
            changeIPandPort()
            true
        } else
            super.onOptionsItemSelected(item)
    }

    private fun changeIPandPort() {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_change_ip_port)
        dialog.setTitle(resources.getString(R.string.change_link))
        val ip = dialog.findViewById<EditText>(R.id.ip)
        val port = dialog.findViewById<EditText>(R.id.port)
        val set = dialog.findViewById<Button>(R.id.set)
        set.setOnClickListener({
            SessionManager.putString(SessionKeys.IP.key, ip.text.toString())
            SessionManager.putString(SessionKeys.PORT.key, port.text.toString())
            dialog.dismiss()
        })
        dialog.show()
    }

    override fun onBackPressed() {
        if (!router.handleBack())
            super.onBackPressed()
    }
}
